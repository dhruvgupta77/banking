Assumptions
1.	The customer data is not maintained and is assumed to be maintained by the invoking application.
2.	As payments is for internal accounts, the currency of account is considered same and hence not considered for payments
3.	JSON is used for input and output

Technology stack
The chosen technologies are
1.	Java 8
2.	Apache http client (for tests)
3.	Maven
4.	JAX-RS
Project building
mvn clean package

Project running
java -jar banking.jar

Testing
1.	Postman or Soap UI can be used to test webservice
2.	URL to invoke web service
http://localhost:8080/banking/webapi/transfer
3.	Sample Input Request
{
"fromAccount": "7891",
"toAccount": "7892",
"amount": 1000
}
4.	A/c range 7891-7895 dummy data has been added for testing


TODO list
1.	Logging
2.	Two-phase payments
3.	Add Currency for International Payment
4.	maven-release-plugin
5.	Junit Test Cases
