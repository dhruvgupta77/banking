package test.javapractice.banking.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import test.javapractice.banking.database.DatabaseClass;
import test.javapractice.banking.model.Account;
import test.javapractice.banking.model.ErrorMessage;
import test.javapractice.banking.model.PaymentMessage;

public class BankingService {

	private static Map<String, Account> accountMap = DatabaseClass.getAllAccounts();

	static {		
		//Creating dummy accounts
		accountMap.put("7891", new Account("7891", 11000));
		accountMap.put("7892", new Account("7892", 12000));
		accountMap.put("7893", new Account("7893", 13000));
		accountMap.put("7894", new Account("7894", 14000));
		accountMap.put("7895", new Account("7895", 15000));
	}

	public BankingService() {
	}

	public List<Account> getAllaccounts() {
		return new ArrayList<Account>(accountMap.values());
	}

	public List<Account> transferFunds(PaymentMessage pytMsg) {		
		Account fromAct = accountMap.get(pytMsg.getFromAccount());
		Account toAct = accountMap.get(pytMsg.getToAccount());
		double amount = pytMsg.getAmount();

		if (fromAct == null || toAct == null) {
			ErrorMessage errorMessage = new ErrorMessage("Either fromAccount or toAccount does not exist", 404,
					"http://localhost:8080/Banking/webapi/transfer");
			Response response = Response.status(Status.NOT_FOUND).entity(errorMessage).build();
			throw new WebApplicationException(response);

		}

		if (fromAct.equals(toAct)) {
			ErrorMessage errorMessage = new ErrorMessage("FromAccount and ToAccount cannot be same", 500,
					"http://localhost:8080/Banking/webapi/transfer");
			Response response = Response.status(Status.NOT_FOUND).entity(errorMessage).build();
			throw new WebApplicationException(response);
		}

		if (fromAct.getBalance() - amount <= 500) {
			ErrorMessage errorMessage = new ErrorMessage("Insufficient Withdrawable balance", 500,
					"http://localhost:8080/Banking/webapi/transfer");
			Response response = Response.status(Status.NOT_FOUND).entity(errorMessage).build();
			throw new WebApplicationException(response);

		} else {
			synchronized (this) {
				fromAct.setBalance(fromAct.getBalance() - amount);
				toAct.setBalance(toAct.getBalance() + amount);
			}
			/*
			 * statusMessage = "COMPLETED: " + " " + amount +
			 * " was successfully transferred from A/C# " +
			 * pytMsg.getFromAccount() + " to A/C# " + pytMsg.getToAccount();
			 */
		}

		List<Account> accountList = new ArrayList<Account>();
		accountList.add(fromAct);
		accountList.add(toAct);
		return accountList;
	}
}