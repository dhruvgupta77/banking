package test.javapractice.banking.database;

import java.util.HashMap;
import java.util.Map;

import test.javapractice.banking.model.Account;

public class DatabaseClass {

    private static Map<String, Account> accountMap = new HashMap<>();

	public static Map<String, Account> getAllAccounts() {
		return accountMap;
	}
}