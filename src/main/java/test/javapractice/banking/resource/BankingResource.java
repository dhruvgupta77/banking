package test.javapractice.banking.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import test.javapractice.banking.model.Account;
import test.javapractice.banking.model.PaymentMessage;
import test.javapractice.banking.service.BankingService;

@Path("transfer")
public class BankingResource {

	private BankingService bankingService = new BankingService();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Account> getMessages() {
		return bankingService.getAllaccounts();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Account> addMessage(PaymentMessage pymtMsg) {
		return bankingService.transferFunds(pymtMsg);
	}
}